<?php
  require_once "animal.php";
  require_once "frog.php";
  require_once "ape.php";

  $sheep  = new Animal("Shaun");
  echo "Release 0"."<br>";
  echo $sheep ->name ."<br>";
  echo $sheep ->legs ."<br>";
  echo $sheep -> cold_blooded."<br>";
  echo "<br>";

  echo "Release 1"."<br>";
  $sungokong = new Ape("Kera Sakti");
  echo $sungokong ->name ."<br>";
  echo $sungokong ->legs ."<br>";
  $sungokong -> yell();
  echo "<br>";
  echo "<br>";

  $kodok = new Frog("buduk");
  echo $kodok ->name ."<br>";
  $kodok -> jump();
?>